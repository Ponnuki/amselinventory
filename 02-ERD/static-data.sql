-- INSERT SCRIPT

-- STATIC DATA
-- UserTypes
INSERT INTO `Inventory`.`UserTypes` (`UserType`) VALUES ('User');
INSERT INTO `Inventory`.`UserTypes` (`UserType`) VALUES ('Manager');
INSERT INTO `Inventory`.`UserTypes` (`UserType`) VALUES ('Administrator');

-- PRODUCT UNIT
INSERT INTO `Inventory`.`ProductUnit` (`ProductUnit`) VALUES ('ขวด');
INSERT INTO `Inventory`.`ProductUnit` (`ProductUnit`) VALUES ('แพ็ค');
INSERT INTO `Inventory`.`ProductUnit` (`ProductUnit`) VALUES ('กล่อง');

-- Transaction Type
INSERT INTO `Inventory`.`TransactionType` (`TransactionType`) VALUES ('Sold');
INSERT INTO `Inventory`.`TransactionType` (`TransactionType`) VALUES ('Purchase');
INSERT INTO `Inventory`.`TransactionType` (`TransactionType`) VALUES ('Withdraw');

-- Issue Status
INSERT INTO `Inventory`.`IssueStatus` (`IssueType`) VALUES ('Open');
INSERT INTO `Inventory`.`IssueStatus` (`IssueType`) VALUES ('Pending');
INSERT INTO `Inventory`.`IssueStatus` (`IssueType`) VALUES ('Close with Fix');
INSERT INTO `Inventory`.`IssueStatus` (`IssueType`) VALUES ('Close ignore');

-- Issue Type
INSERT INTO `Inventory`.`IssueType` (`Issue`) VALUES ('Stock');
INSERT INTO `Inventory`.`IssueType` (`Issue`) VALUES ('Customer Complain');
INSERT INTO `Inventory`.`IssueType` (`Issue`) VALUES ('Staff');
INSERT INTO `Inventory`.`IssueType` (`Issue`) VALUES ('Store');
INSERT INTO `Inventory`.`IssueType` (`Issue`) VALUES ('Others');



-- INSERT INTO `Users`
-- Test User
INSERT INTO `Inventory`.`Users` (`FirstName`, `LastName`, `NickName`, `RegisteredDate`, `Updated`, `UserName`, `Password`, `UserTypeID`)
VALUES ('test', 'test', 'test', '2017-03-15', '2017-03-15', 'test', MD5('test'), '3');

INSERT INTO `Inventory`.`Users` (`FirstName`, `LastName`, `NickName`, `RegisteredDate`, `Updated`, `UserName`, `Password`, `UserTypeID`)
VALUES ('พรเจตน์', 'ศักดิ์กิจจรุง', 'PAO', '2017-03-15', '2017-03-15', 'pornjeds', MD5('xvoo^db318'), '3');

-- INSERT INTO `Products`
INSERT INTO `Inventory`.`Products` (`ProductName`, `ProductImage`, `Price`, `Commission`, `Updated`, `ProductUnit`, `canRedeem`)
VALUES ('Glutamine 800', NULL, 400 , 0, '2017-03-15', 1, 0);

INSERT INTO `Inventory`.`Products` (`ProductName`, `ProductImage`, `Price`, `Commission`, `Updated`, `ProductUnit`, `canRedeem`)
VALUES ('Amino Collagen', NULL, 320 , 0, '2017-03-15', 1, 0);
